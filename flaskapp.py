import os

from flask import Flask
from peewee import PostgresqlDatabase
from flask_login import LoginManager
from flask_assets import Environment, Bundle
from flask_mail import Mail
from werkzeug.debug import DebuggedApplication

app = Flask(__name__)

app.debug = True
app.config.from_pyfile('flaskapp.cfg')
app.wsgi_app = DebuggedApplication(app.wsgi_app, True)

app.config.setdefault('MAIL_PORT', 587)
app.config.setdefault('MAIL_SERVER', os.environ["MAIL_SERVER"])
app.config.setdefault('MAIL_USERNAME', os.environ["MAIL_USERNAME"])
app.config.setdefault('MAIL_PASSWORD', os.environ["MAIL_PASSWORD"])
app.config.setdefault('MAIL_USE_TLS', True)

login_manager = LoginManager(app)
mail = Mail(app)

assests = Environment(app)
css_base = Bundle("css/base.css", "css/forms.css", "css/post_styles.css", filters='cssmin', output='gen/compressed.css')
assests.register("css_base", css_base)
js_base = Bundle("js/forms.js", "js/validate.js", filters='jsmin', output='gen/compressed.js')
assests.register("js_base", js_base)

db = PostgresqlDatabase("nar", port=os.environ['OPENSHIFT_POSTGRESQL_DB_PORT'],
                        host=os.environ['OPENSHIFT_POSTGRESQL_DB_HOST'],
                        user=os.environ["OPENSHIFT_POSTGRESQL_DB_USERNAME"],
                        password=os.environ["OPENSHIFT_POSTGRESQL_DB_PASSWORD"])

from frontend_views import *
# noinspection PyUnresolvedReferences
from app_resources import *
from api import *

if __name__ == '__main__':
    app.run()
