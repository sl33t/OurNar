#Nar
Welcome to OurNar. We are in the beginning stages of development and have a long way to go.

Right now to set up our project you need an smtp server for flask mail and postgres installed locally and will need to set a few environment variables in your system and create a few database tables.

#Enviorment Variables
+ MAIL_SERVER: the an smtp server
+ MAIL_USERNAME: your username or email address
+ MAIL_PASSWORD: mail password
+ OPENSHIFT_POSTGRESQL_DB_PORT: the postgres port
+ OPENSHIFT_POSTGRESQL_DB_HOST: the postgres host aka 127.0.0.1
+ OPENSHIFT_POSTGRESQL_DB_USERNAME: your postgres username
+ OPENSHIFT_POSTGRESQL_DB_PASSWORD: your postgres password

#DB Tables
+ Wal
+ Comment
+ Post
+ SubModRelationship
+ SubscribedRelationship
+ User

#Key Points
+ Transparency between admins, mods, and users.
+ The ability to create, vote on and submit content.
+ The idea that sites should be clean without messy designs or urls.

#Restrictions
+ No pornographic content of someone under 18 years.
+ Pornography, graphic violence, and gore filled content must be marked as such. 
Tags include [Porn], [Gore], and  [Violence](Not yet implimented.

#Ideas for the future of Nar
+ Specific set of rules required for each wal (as much or as little as they please).
+ No ex post facto rules(after the fact). Ex: Something is done wrong but no rule in place, add rule, remove content, user goes unpunished.
+ Explanation required for bans. Insufficient explanations can be appealed.
+ Strong moderator tools are a must (prevent briggading).
+ Doxxing or any kind of real life assault of any kind (physical, sexual, or destroying someones career or personal life) will not be tolerated.
+ Public wals until an application for a private wal is made. (50 or more users?)
+ Private subs can chose to operate as fully closed or viewable but with interaction by invitation only.
+ Bans automatically remove all user content including posts and comments from a wal (wal only not full site bans).
+ Automod feature which instead of removing them completly, hides them and enters them into a que for removal or reinstatement.
+ All removed content is able to be seen but only when expressly asked for.
+ Ability for mods to move content to another wal with permission from a mod of each and the users consent (Comments are attaached to the post instead of the wal.