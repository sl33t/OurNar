from setuptools import setup

setup(name='OurNar',
      version='.01',
      description='OurNar: A social site.',
      author='Ricky Catron',
      author_email='catron.ricky@gmail.com',
      install_requires=['Flask>=0.10.1', 'peewee', 'flask_login', 'markdown', 'Flask-Assets', 'cssmin', 'jsmin',
                        'Jinja2>=2.8', 'flask_mail', 'flask-wtf'],
      )
