import datetime
from hashlib import sha512
import json

from flask import request, redirect, abort, jsonify, flash
from flask.ext.login import current_user, login_user
from flask.views import MethodView
import peewee
from playhouse.shortcuts import model_to_dict

from app_resources import back
from flaskapp import app
from forms import RegisterForm, WalForm, PostForm
from models import User, Wal, Post, Comment
import nar_utils


class NewWal(MethodView):
    def get(self, name=""):
        if name != "":
            return jsonify(
                Wal.select(Wal.id, Wal.name, Wal.nsfw, Wal.sensative, Wal.description, Wal.side_bar, Wal.admin_only,
                           Wal.is_front_page, Wal.open_sealed_closed).where(
                    Wal.name == name).dicts().get())
        else:
            return json.dumps([model_to_dict(sub, exclude=[]) for sub in
                               Wal.select(Wal.mods, Wal.subscribed, Wal.id, Wal.name, Wal.nsfw, Wal.sensative,
                                          Wal.description, Wal.side_bar, Wal.admin_only, Wal.is_front_page,
                                          Wal.open_sealed_closed)])

    def post(self):
        if current_user.is_authenticated():
            form = WalForm(request.form)
            if form.validate():
                try:
                    if form.classification.data == "sensative":
                        new_wal = Wal(name=form.wal_name.data.lower(), side_bar="", admin_only=False,
                                      open_sealed_closed=int(form.protection.data),
                                      description=form.description.data, nsfw=False, is_front_page=False,
                                      sensative=True,
                                      subscribed=[],
                                      mods=[int(current_user.get_id())])
                    elif form.classification.data == "nsfw":
                        new_wal = Wal(name=form.wal_name.data, side_bar="", admin_only=False,
                                      open_sealed_closed=int(form.protection.data),
                                      description=form.description.data, nsfw=True, is_front_page=False,
                                      sensative=False,
                                      subscribed=[],
                                      mods=[int(current_user.get_id())])
                    else:
                        new_wal = Wal(name=form.wal_name.data, side_bar="", admin_only=False,
                                      open_sealed_closed=int(form.protection.data),
                                      description=form.description.data, nsfw=False, is_front_page=False,
                                      sensative=False,
                                      subscribed=[],
                                      mods=[int(current_user.get_id())])
                    new_wal.save()
                except peewee.IntegrityError:
                    flash("Wal Name Taken")
                    return redirect(back())
            else:
                flash("Form Invalid")
                return redirect(back())
            return redirect("/wal/" + form.wal_name)
        return abort(403)


user_view = NewWal.as_view('api/wal')
app.add_url_rule('/api/wal/<name>', view_func=user_view, methods=['GET', ])
app.add_url_rule('/api/wal', view_func=user_view, methods=['POST', 'GET'])


class UserAPI(MethodView):
    def get(self, username=""):
        if username != "":
            try:
                return jsonify(User.select(User.id, User.username, User.up_votes, User.down_votes, User.tusks).where(
                    User.username == username).dicts().get())
            except:
                return abort(404)
        else:
            return json.dumps([model_to_dict(user, exclude=[User.email, User.password, User.theme]) for user in
                               User.select(User.id, User.username, User.up_votes, User.down_votes, User.tusks,
                                           User.subscribed, User.is_admin)])

    def post(self):
        form = RegisterForm(request.form)
        if form.validate():
            try:
                new_user = User(username=form.username.data, password=sha512(form.password.data).hexdigest(),
                                up_votes=0,
                                down_votes=0, tusks=0, is_admin=False,
                                theme=0, email=form.email.data, subscribed=[])
                new_user.save()
            except peewee.IntegrityError:
                flash("Username Taken")
                return redirect(back())
            login_user(new_user)
        else:
            flash("Please fill all fields")
        return redirect(back())


user_view = UserAPI.as_view('api/user')
app.add_url_rule('/api/users', view_func=user_view, methods=['GET', 'POST'])
app.add_url_rule('/api/users/<username>', view_func=user_view, methods=['GET', ])


class PostAPI(MethodView):
    def get(self, id=""):
        if id != "":
            try:
                return jsonify(Post.select(Post.id, Post.title, Post.text, Post.url, Post.up_votes, Post.down_votes,
                                           Post.tusks, Post.sub, Post.user, Post.date).where(
                    Post.id == int(id)).dicts().get())
            except:
                return abort(404)
        else:
            return json.dumps([model_to_dict(post, exclude=[Post.date, Post.user]) for post in
                               Post.select(Post.id, Post.title, Post.text, Post.url, Post.up_votes, Post.down_votes,
                                           Post.tusks, Post.sub)])

    def post(self):
        if current_user.is_authenticated():
            walname = request.form["wal_name"]
            form = PostForm(request.form)
            if form.validate():
                if form.text.data != "" or form.url.data != "":
                    new_post = Post(title=form.title.data, up_votes=0, down_votes=0, tusks=0, text=form.text.data,
                                    date=datetime.datetime.now(),
                                    user=User.get(User.id == current_user.get_id()),
                                    url=form.url.data, sub=Wal.get(Wal.name == walname))
                    new_post.save()
                return redirect("/post/" + walname + "/" + str(new_post.get_id()))
            else:
                flash("Please fill in all fields")
                return redirect(back())
        else:
            return abort(403)


post_view = PostAPI.as_view('api/posts')
app.add_url_rule('/api/posts/<id>', view_func=post_view, methods=['GET', ])
app.add_url_rule('/api/posts', view_func=post_view, methods=['POST', 'GET'])


class CommentAPI(MethodView):
    def get(self, id=""):
        if id != "":
            try:
                return jsonify(Comment.select(Comment.id, Comment.post, Comment.text, Comment.user, Comment.up_votes,
                                              Comment.down_votes,
                                              Comment.tusks, Comment.date).where(
                    Post.id == int(id)).dicts().get())
            except:
                return abort(404)

    def post(self):
        if current_user.is_authenticated():
            walname = request.form["walname"]
            post_id = request.form["post_id"]
            text = request.form["text"]
            parent = request.form["parent"]

            new_comment = nar_utils.comments.new_comment(text, post_id, parent);

            return redirect("/post/" + walname + "/" + post_id + "#" + str(new_comment.id))
        else:
            return abort(403)


post_view = CommentAPI.as_view('api/comments')
app.add_url_rule('/api/comments/<id>', view_func=post_view, methods=['GET', ])
app.add_url_rule('/api/comments', view_func=post_view, methods=['POST', ])
