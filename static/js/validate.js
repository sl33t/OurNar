function validate_form(form, formid) {
    var parent_form = document.getElementById(form);
    for (var child_number in parent_form.childNodes) {
        var child = parent_form.children[child_number];
        if ((child.tagName == 'TEXTAREA') || (child.tagName == 'INPUT' && (child.type == "text" || child.type == "password"))) {
            if (child.value == "") {
                document.getElementById(formid).children[2].innerText = "Please fill in all fields.";
                return false;
            }
        }
    }
    return true;
}