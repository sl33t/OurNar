function showForm(form_name) {
    document.getElementById('login').style.display = 'none';
    document.getElementById('register').style.display = 'none';
    document.getElementById('forgot_password').style.display = 'none';

    document.getElementById('dark_cover').style.display = 'block';
    document.getElementById(form_name).style.display = 'block';
}

function closeForm(form_name) {
    document.getElementById('dark_cover').style.display = 'none';
    document.getElementById(form_name).children[2].innerText = "";
    document.getElementById(form_name).style.display = 'none';
}

function post(path, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

function show_comment(parent) {
    for (var child_number in parent.childNodes) {
        var child = parent.children[child_number];
        if (child.nodeName == "FORM") {
            for (var inner_child_number in child.childNodes) {
                var inner_child = child.children[inner_child_number];
                inner_child.style.display = "block";
            }
        }

    }
}