from flask import render_template, abort, Markup, flash, request, redirect
from flask.ext.login import current_user, login_required, logout_user
from flask.ext.mail import Message
import markdown
import peewee

from app_resources import back
from flaskapp import app, mail
from models import Wal, Post, User, Comment
import nar_utils
from forms import BaseForms, LoginForm, ForgotPasswordForm


@app.route('/')
@app.route("/home")
@app.route("/home/<page_number>")
def index(page_number="1"):
    context = {'admins': User.select().where(User.is_admin),
               "hot": nar_utils.posts.get_hot_posts(int(page_number) - 1),
               'recent': nar_utils.posts.get_recent_posts(), 'form': BaseForms()}
    return render_template('index.html', **context)


@app.route("/wal/<wal_name>")
@app.route("/wal/<wal_name>/<page_number>")
def wal(wal_name, page_number="0"):
    try:
        specific_wal = Wal.get(name=wal_name.lower())
    except:
        abort(404)
    mods = []
    for mod in specific_wal.mods:
        mods.insert(0, User.get(User.id == mod))
    context = {'wal': specific_wal,
               'posts': nar_utils.posts.get_hot_posts(int(page_number), specific_wal),
               'Markup': Markup,
               'markdown': markdown,
               'mods': mods,
               'subscribed': current_user.get_id() in specific_wal.subscribed,
               'recent': nar_utils.posts.get_recent_posts(specific_wal),
               'form': BaseForms()}
    return render_template('wal.html', **context)


@app.route("/post/<wal_name>/<post_id>")
def post(wal_name, post_id):
    try:
        specific_wal = Wal.get(name=wal_name.lower())
        specific_post = Post.get(Post.id == post_id)
    except peewee.DoesNotExist:
        return abort(404)
    context = {'wal': specific_wal,
               'post': specific_post,
               'Markup': Markup,
               'markdown': markdown,
               'comments': Comment.select().where(Comment.comments is None and Comment.post == specific_post),
               'form': BaseForms()}
    return render_template('post.html', **context)


@app.route("/user/<username>")
def user_page(username):
    try:
        user = User.get(User.username == username)
    except peewee.DoesNotExist:
        return abort(404)
    context = {'user': user,
               'form': BaseForms()}
    return render_template("user.html", **context)


@app.route("/settings/<username>")
def settings_page(username):
    if current_user.is_authenticated() and current_user.username == username:
        try:
            user = User.get(User.username == username)
        except peewee.DoesNotExist:
            return abort(404)
        context = {'user': user,
                   'form': BaseForms()}
        return render_template("settings.html", **context)
    return abort(403)


@app.route("/login", methods=["POST"])
def login():
    form = LoginForm(request.form)
    if not form.validate():
        flash("Username or password is incorrect")
    return redirect(back())


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(back())


@app.route("/subscribe", methods=["POST"])
@login_required
def subscribe():
    try:
        wal = Wal.get(Wal.name == request.form["walname"].lower())
    except:
        return abort(404)
    if request.form["subscribe"].lower() != "subscribe":
        wal.subscribed.remove(current_user.get_id())
        user = User.get(User.id == current_user.get_id())
        user.subscribed.remove(wal.id)
        wal.save()
        user.save()
        return redirect("/wal/" + wal.name)
    else:
        wal.subscribed.insert(0, int(current_user.get_id()))
        user = User.get(User.id == current_user.get_id())
        user.subscribed.insert(0, int(wal.id))
        wal.save()
        user.save()
    return redirect("/")


@app.route("/vote", methods=["POST"])
@login_required
def vote():
    up_down = request.form["up_down"]
    post_type = request.form["type"]
    post_id = request.form["id"]

    if post_type == "comment":
        comment = Comment.get(Comment.id == int(post_id))
        if up_down == "up":
            comment.up_votes += 1
            comment.save()
        else:
            comment.down_votes += 1
            comment.save()
        return redirect("/post/" + comment.post.sub.name + "/" + comment.post.id + "#" + str(comment.id))
    elif post_type == "post":
        specific_post = Post.get(Post.id == int(post_id))
        if up_down == "up":
            specific_post.up_votes += 1
            specific_post.save()
        else:
            specific_post.down_votes += 1
            specific_post.save()
        return redirect("/post/" + post.sub.name + "/" + post.id)


@app.route("/forgot_password", methods=["POST"])
def forgot_password():
    form = ForgotPasswordForm(request.form)
    if form.validate():
        try:
            user = User.get(User.username == form.username.data)
        except:
            flash("User not found")
            return abort(404)

        msg = Message("OurNar Password Reset",
                      sender=("OurNar", app.config["MAIL_USERNAME"]),
                      recipients=[user.email])

        msg.html = "Password Reset coming soon!"
        mail.send(msg)
        flash("Email Sent")
    else:
        flash("Form Invalid")
    return redirect("/")
