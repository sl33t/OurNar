from hashlib import sha512

from flask.ext.login import login_user
from wtforms import Form
import peewee
from wtforms.fields.core import StringField, RadioField
from wtforms import validators
from wtforms.fields.html5 import EmailField, URLField
from wtforms.fields.simple import PasswordField, TextAreaField
from wtforms.validators import EqualTo

from models import User


class LoginForm(Form):
    username = StringField(label="Username:", validators=[validators.InputRequired()])
    password = PasswordField(label="Password:", validators=[validators.InputRequired()])

    def validate(self):
        form = Form.validate(self)
        if not form:
            return False
        try:
            user = User.get(User.username == self.username.data)
            if user.password == sha512(self.password.data).hexdigest():
                login_user(user)
            else:
                return False
        except peewee.DoesNotExist:
            return False
        return True


class RegisterForm(Form):
    username = StringField(label="Username:", validators=[validators.InputRequired()])
    password = PasswordField(label="Password:", validators=[validators.InputRequired(), EqualTo('confirm_password',
                                                                                                message='Passwords must match')])
    confirm_password = PasswordField(label="Confirm Password:", validators=[validators.InputRequired()])
    email = EmailField(label="Email Address:", validators=[validators.InputRequired(), validators.Email()])


class ForgotPasswordForm(Form):
    username = StringField("Username:", validators=[validators.InputRequired()])


class WalForm(Form):
    wal_name = StringField(label="Wal Name:", validators=[validators.InputRequired()])
    description = TextAreaField(label="Description:", validators=[validators.InputRequired()])
    classification = RadioField(choices=[('neither', 'Neither'), ('nsfw', 'NSFW'), ('sensative', 'Sensative')],
                                default='neither')
    protection = RadioField(choices=[('0', 'Open'), ('1', 'Closed'), ('2', 'Sealed')], default='0')


class PostForm(Form):
    title = StringField(label="Title:", validators=[validators.InputRequired()])
    text = TextAreaField(label="Text:", validators=[])
    url = URLField(label="Url:", validators=[validators.URL(message="Link cannot be found")])


class BaseForms():
    def __init__(self):
        self.WalForm = WalForm()
        self.ForgotPasswordForm = ForgotPasswordForm()
        self.RegisterForm = RegisterForm()
        self.LoginForm = LoginForm()
        self.PostForm = PostForm()
