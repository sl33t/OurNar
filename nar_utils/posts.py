import datetime
import operator

from flask.ext.login import current_user

from models import Post, Wal, User


def get_hot_posts(page, sub=""):
    total_sub_posts = []
    if sub == "":
        if current_user.is_authenticated():
            front_page_subs = []
            subscribed_rel = User.get(User.id == current_user.get_id()).subscribed
            for rel in subscribed_rel:
                front_page_subs.insert(0, Wal.get(Wal.id == rel))
        else:
            front_page_subs = Wal.select().where(Wal.is_front_page == True)
        for sub in front_page_subs:
            sub_posts = get_recent_posts(sub, 100)
            for sub_post in sub_posts:
                total_sub_posts.append(sub_post)
    else:
        sub_posts = get_recent_posts(sub, 100)
        for sub_post in sub_posts:
            total_sub_posts.append(sub_post)
    dictionary = {}
    date = datetime.datetime.now().hour
    for post in total_sub_posts:
        difference = post.date.hour - date
        if difference == 0:
            difference = 1
        dictionary[post] = (post.up_votes - post.down_votes) / difference
    tuple_list = (sorted(dictionary.items(), key=operator.itemgetter(1)))[page * 10:(page * 10) + 25]
    return [item[0] for item in tuple_list]


def get_recent_posts(sub="", count=5, page_number="0"):
    if sub != "":
        return Post.select().where(Post.sub == sub).order_by(Post.date.desc()).limit(count)
    else:
        sub = Wal.select().where(Wal.is_front_page == True)
        return Post.select().where(Post.sub == sub).order_by(Post.id.desc()).paginate(int(page_number), count)
