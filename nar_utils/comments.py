import datetime
import operator

from flask.ext.login import current_user

from flaskapp import db
from models import Comment, User, Post


def new_comment(text, post_id, parent):
    db.connect()
    user = User.get(User.username == current_user.username)
    post = Post.get(Post.id == int(post_id))
    if parent == "":
        parent = None
    comment = Comment(user=user, post=post, comments=parent, text=text, date=datetime.datetime.now())
    comment.save()
    db.close()
    return comment


def get_hot_comments(head_comment):
    dictionay = {}
    date = datetime.datetime.now().minute
    for comment in head_comment:
        difference = comment.date.minute - date
        if difference == 0:
            difference = 1
        dictionay[comment] = (comment.up_votes - comment.down_votes) / difference
    touple_list = (sorted(dictionay.items(), key=operator.itemgetter(1)))
    return [item[0] for item in touple_list]
