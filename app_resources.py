from flask import send_from_directory, request, render_template

from flaskapp import login_manager, app, db
from models import User
import nar_utils.comments as comments


@app.route('/<path:resource>')
def serveStaticResource(resource):
    return send_from_directory('static/', resource)


@login_manager.user_loader
def load_user(userid):
    return User.get(User.id == userid)


@app.before_request
def before_request():
    db.connect()


@app.teardown_request
def teardown_request(exception):
    if not db.is_closed():
        db.close()


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404


def back():
    return request.args.get('next') or request.referrer


app.jinja_env.globals.update(get_hot_comments=comments.get_hot_comments)
