#!/usr/bin/env python

import imp
import os
import sys

try:
    virtenv = os.path.join(os.environ.get('OPENSHIFT_PYTHON_DIR', '.'), 'virtenv')
    python_version = "python" + str(sys.version_info[0]) + "." + str(sys.version_info[1])
    os.environ['PYTHON_EGG_CACHE'] = os.path.join(virtenv, 'lib', python_version, 'site-packages')
    virtualenv = os.path.join(virtenv, 'bin', 'activate_this.py')
    if (sys.version_info[0] < 3):
        execfile(virtualenv, dict(__file__=virtualenv))
    else:
        exec (open(virtualenv).read(), dict(__file__=virtualenv))

except IOError:
    pass

#
# IMPORTANT: Put any additional includes below this line.  If placed above this
# line, it's possible required libraries won't be in your searchable path
#

if __name__ == '__main__':
    from flaskapp import app as application

    port = application.config['PORT']
    ip = application.config['IP']
    app_name = application.config['APP_NAME']
    host_name = application.config['HOST_NAME']

    fwtype = "wsgiref"
    try:
        imp.find_module("flask")
        fwtype = "flask"
    except ImportError:
        pass

    print('Starting WSGIServer type %s on %s:%d ... ' % (fwtype, ip, port))
    if fwtype == "flask":
        from flask import Flask

        server = Flask(__name__)
        server.wsgi_app = application
        server.run(host=ip, port=port)

    else:
        from wsgiref.simple_server import make_server

        make_server(ip, port, application.app).serve_forever()
