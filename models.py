from peewee import *
from playhouse.postgres_ext import ArrayField

from flaskapp import db


class BaseModel(Model):
    class Meta:
        database = db


class CanVoteOn(BaseModel):
    up_votes = BigIntegerField()
    down_votes = BigIntegerField()
    tusks = BigIntegerField()


class UserContent(CanVoteOn):
    text = TextField()
    date = DateTimeField()


class User(CanVoteOn):
    username = CharField(unique=True)
    password = CharField()
    is_admin = BooleanField()
    theme = IntegerField()
    email = CharField()
    subscribed = ArrayField(IntegerField)

    def as_json(self):
        return dict(
            id=self.id, username=self.username,
            upvotes=self.up_votes,
            downvotes=self.down_votes,
            tusks=self.tusks)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)


class Wal(BaseModel):
    name = CharField(unique=True)
    nsfw = BooleanField()
    sensative = BooleanField()
    description = TextField()
    is_front_page = BooleanField()
    side_bar = TextField()
    open_sealed_closed = IntegerField()
    admin_only = BooleanField()
    subscribed = ArrayField(IntegerField)
    mods = ArrayField(IntegerField)


class Post(UserContent):
    title = CharField()
    user = ForeignKeyField(User, related_name="posts")
    url = CharField(null=True)
    sub = ForeignKeyField(Wal, related_name='posts')


class Comment(UserContent):
    user = ForeignKeyField(User, related_name="user_comments")
    post = ForeignKeyField(Post, related_name='post_comments')
    comments = ForeignKeyField('self', related_name='nested_comments', null=True)
